####resource create virtual network
resource "azurerm_virtual_network" "myvnet" {
  name = "myvnet-1"
  address_space = ["10.0.0.0/16"]
  ##3to map the vnet we need to add azurerg with location and name
  location = azurerm_resource_group.demo-rg-1.location
  resource_group_name = azurerm_resource_group.demo-rg-1.name 
}
#####inside virtual network subnet
resource "azurerm_subnet" "mysubnet" {
   name = "mysubnet"
  ##3to map the vnet we need to add azurerg with location and name
  #location = azurerm_resource_group.demo-rg-1.location
  resource_group_name = azurerm_resource_group.demo-rg-1.name 
  virtual_network_name =  azurerm_virtual_network.myvnet.name
  address_prefixes = [ "10.0.2.0/24" ]
}

##create a public ip
resource "azurerm_public_ip" "mypublicip" {
    count = 2
    ##an explicit dependency to have this resource created only after vnet and subnet is creted
    
  name = "mypublicip-${count.index}"
  ##3to map the vnet we need to add azurerg with location and name
  location = azurerm_resource_group.demo-rg-1.location
  resource_group_name = azurerm_resource_group.demo-rg-1.name 
  allocation_method   = "Static"
  domain_name_label = "app1-vm-${count.index}${random_string.myrandom.id}"
}

##we will create the nic interface 
resource "azurerm_network_interface" "myvmnic" {
  count = 2
  name = "myvmnic-${count.index}"
  ##3to map the vnet we need to add azurerg with location and name
  location = azurerm_resource_group.demo-rg-1.location
  resource_group_name = azurerm_resource_group.demo-rg-1.name 
   ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.mysubnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = element(azurerm_public_ip.mypublicip[*].id, count.index)
  }
}



##3create the nic and attach the public ip with nic