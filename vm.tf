resource "azurerm_linux_virtual_machine" "mylinuxvm" {
    count = 2
  name                = "mylinuxvm-${count.index}"
  computer_name = "testlinux-${count.index}"
  location = azurerm_resource_group.demo-rg-1.location
  resource_group_name = azurerm_resource_group.demo-rg-1.name
  size                = "Standard_DS1_v2"
  admin_username      = "azureuser"
  network_interface_ids = [
    element(azurerm_network_interface.myvmnic[*].id, count.index)
  ]

  admin_ssh_key {
    username   = "azureuser"
    public_key = file("${path.module}/ssh-keys/terraform.pub")
  }

  os_disk {
    name = "osdisk${count.index}"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "REDHAT"
    offer     = "RHEL"
    sku       = "83-gen2"
    version   = "latest"
  }
  custom_data = filebase64("${path.module}/app-scripts/app.txt")
}
