####terraform settings block
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
      
    }
    random = {
        source = "hashicorp/random"
      }
  }
}
###when i run terraform init
####create a folder in your project folder .terraform inside this it download all the api required to communicate with your cloud service provider

###configure the microsoft azure provider
provider "azurerm" {
  features {}
}

resource "random_string" "myrandom" {
  length = 6 
  upper = false 
  special = false 
  numeric = false 
}